# What is new since

What are the new features since GitLab release x?

Renders a table of released features per GitLab release.

Parsed from https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data%2Frelease_posts

# Usage

- Run via `python3 gitlab_feature_report.py`
- produces html table representation of the features in the `public` folder
- Run via GitLab CI to deploy the feature overview via GitLab pages
